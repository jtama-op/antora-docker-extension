'use strict'

const fs = require('fs')
const { promises: fsp } = fs
const ospath = require('path')
const { PassThrough } = require('stream')
const { spawn } = require('child_process')
const {
  GenericContainer,
  Wait,
} = require('testcontainers')

const IS_WIN = process.platform === 'win32'
const DBL_QUOTE_RX = /"/g
const QUOTE_RX = /["']/

// adapted from https://github.com/jpommerening/node-lazystream/blob/master/lib/lazystream.js | license: MIT
class LazyReadable extends PassThrough {
  constructor (fn, options) {
    super(options)
    this._read = function () {
      delete this._read // restores original method
      fn.call(this, options).on('error', this.emit.bind(this, 'error')).pipe(this)
      return this._read.apply(this, arguments)
    }
    this.emit('readable')
  }
}

const fileExists = (p) =>
  fsp.access(p).then(
    () => true,
    () => false
  )

const parseCommand = (cmd) => {
  if (!QUOTE_RX.test(cmd)) return cmd.split(' ')
  const chars = [...cmd]
  const lastIdx = chars.length - 1
  return chars.reduce(
    (accum, c, idx) => {
      const { tokens, token, quotes } = accum
      if (c === "'" || c === '"') {
        if (quotes.get()) {
          if (quotes.get() === c) {
            if (token[token.length - 1] === '\\') {
              token.pop()
              token.push(c)
            } else {
              if (token.length) tokens.push(token.join(''))
              token.length = quotes.clear() || 0
            }
          } else {
            token.push(c)
          }
        } else {
          quotes.set(undefined, c)
        }
      } else if (c === ' ') {
        if (quotes.get()) {
          token.push(c)
        } else if (token.length) {
          tokens.push(token.join(''))
          token.length = 0
        }
      } else {
        token.push(c)
      }
      if (idx === lastIdx && token.length) tokens.push(token.join(''))
      return accum
    },
    { tokens: [], token: [], quotes: new Map() }
  ).tokens
}

const winShellEscape = (val) => {
  if (val.charAt() === '-') return val
  if (~val.indexOf('"')) {
    return ~val.indexOf(' ') ? `"${val.replace(DBL_QUOTE_RX, '"""')}"` : val.replace(DBL_QUOTE_RX, '""')
  }
  if (~val.indexOf(' ')) return `"${val}"`
  return val
}

async function runCommand (cmd, argv = [], opts = {}) {
  if (!cmd) throw new TypeError('Command not specified')
  let cmdv = parseCommand(cmd)
  const { input, output, quiet, implicitStdin, local, ...spawnOpts } = opts
  if (input) input instanceof Buffer ? implicitStdin || argv.push('-') : argv.push(input)
  if (IS_WIN) {
    if (local && !cmdv[0].endsWith('.bat')) {
      const cmd0 = `${cmdv[0]}.bat`
      if (await fileExists(ospath.join(opts.cwd || '', cmd0))) cmdv[0] = cmd0
    }
    cmdv = cmdv.map(winShellEscape)
    argv = argv.map(winShellEscape)
    Object.assign(spawnOpts, { shell: true, windowsHide: true })
  } else if (local) {
    cmdv[0] = `./${cmdv[0]}`
  }
  return new Promise((resolve, reject) => {
    const stdout = []
    const stderr = []
    const ps = spawn(cmdv[0], [...cmdv.slice(1), ...argv], spawnOpts)
    ps.on('close', (code) => {
      if (code === 0) {
        if (stderr.length) process.stderr.write(stderr.join(''))
        if (output) {
          output === true ? resolve() : resolve(new LazyReadable(() => fs.createReadStream(output)))
        } else {
          resolve(Buffer.from(stdout.join('')))
        }
      } else {
        let msg = `Command failed: ${ps.spawnargs.join(' ')}`
        if (stderr.length) msg += '\n' + stderr.join('')
        reject(new Error(msg))
      }
    })
    ps.on('error', (err) => reject(err.code === 'ENOENT' ? new Error(`Command not found: ${cmdv.join(' ')}`) : err))
    ps.stdout.on('data', (data) => (output ? !quiet && process.stdout.write(data) : stdout.push(data)))
    ps.stderr.on('data', (data) => stderr.push(data))
    try {
      input instanceof Buffer ? ps.stdin.end(input) : ps.stdin.end()
    } catch (err) {
      reject(err)
    } finally {
      ps.stdin.end()
    }
  })
}

async function runDocker (image, cmd, waitUntil, opts, logger) {
  if (!cmd) throw new TypeError('Command not specified')
  if (!image) throw new TypeError('Image not specified')
  const cmdv = parseCommand(cmd)
  logger.info(waitUntil)
  const container = await new GenericContainer(image)
    .withCommand(cmdv)
    .withWorkingDir('/sources')
    .withWaitStrategy(Wait.forLogMessage(waitUntil.message))
    .withStartupTimeout(waitUntil.timeout)
    .withBindMounts([{
      source: opts.cwd,
      target: '/sources',
    }])
    .start()
  logger.info(container.getId())
  logger.info(container.getName())
  logger.info(cmdv)
  return new Promise((resolve, reject) => {
    resolve()
  })
}

module.exports = {
  runCommand,
  runDocker,
}
