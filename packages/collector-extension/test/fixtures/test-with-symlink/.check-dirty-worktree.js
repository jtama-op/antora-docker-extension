'use strict'

const { promises: fsp } = require('fs')
const { spawnSync } = require('child_process')

;(async () => {
  const untracked = spawnSync('git', ['status', '--short'])
    .stdout.toString()
    .split('\n')
    .filter((it) => it.trimStart().startsWith('?'))
    .join('\n')
    .trimEnd()
  const modified = spawnSync('git', ['diff', '--name-status']).stdout.toString().trimEnd()
  const dirty = untracked + modified
  await fsp.mkdir('build/modules/ROOT/pages', { recursive: true })
  if (dirty) {
    fsp.writeFile('build/modules/ROOT/pages/dirty-worktree.adoc', `= Dirty Worktree\n\n....\n${dirty}\n....`, 'utf8')
  }
})()
